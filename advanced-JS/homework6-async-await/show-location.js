class ShowUserLocation {
  constructor(userObj) {
    this.continent = userObj.continent;
    this.country = userObj.country;
    this.region = userObj.regionName;
    this.city = userObj.city;
    this.district = userObj.district;
  }

  render() {
    this.rootDiv = document.createElement("div");
    this.rootDiv.classList.add("location-box");
    const location = document.createElement("p");
    location.innerText = `continent: ${this.continent}, country: ${this.country}, region: ${this.region}, city: ${this.city}, district: ${this.district}`;
    this.rootDiv.append(location);
    return this.rootDiv;
  }
}

export default ShowUserLocation;
