import ShowUserLocation from "./show-location.js";

const findBtn = document.querySelector(".find-btn");

findBtn.addEventListener("click", async () => {
  const response = await fetch(`https://api.ipify.org/?format=json`);
  const dataIp = await response.json();
  console.log(dataIp.ip);
  const userPromise = await fetch(
    `http://ip-api.com/json/?fields=continent,country,region,regionName,city,district`
  );
  const user = await userPromise.json();

  const userCard = new ShowUserLocation(user);
  document.body.append(userCard.render());
});
