const root = document.createElement("div");
document.body.append(root);
const list = document.createElement("ul");

const getGilms = async () => {
  const response = await fetch("https://ajax.test-danit.com/api/swapi/films");
  const dataFilms = await response.json();
  dataFilms.map((objFilm) => {
    let listItem = document.createElement("li");
    listItem.innerHTML = `EPISODE - ${objFilm.episodeId} </br></br>  NAME -  '${objFilm.name}' </br></br>  SUMMARY - '${objFilm.openingCrawl}' </br>`;
    const animation = document.createElement("div");
    animation.classList.add("loader");

    list.append(listItem);

    const actorsPromises = objFilm.characters.map(async (actor) => {
      const objActor = await fetch(actor);
      const data = await objActor.json();
      return data.name;
    });

    listItem.append(animation);

    Promise.all(actorsPromises).then((data) => {
      const actorsBox = document.createElement("p");
      animation.style.display = "none";
      listItem.append(actorsBox);
      actorsBox.innerText = `ACTORS: ${data}`;
    });
  });
};

root.append(list);

getGilms();

// --------------- SECOND WAY ----------------

// const root = document.createElement("div");
// document.body.append(root);
// const list = document.createElement("ul");

// fetch(`https://ajax.test-danit.com/api/swapi/films`)
//   .then((response) => response.json())
//   .then((films) => {
//     films.map((objFilm) => {
//       let listItem = document.createElement("li");
//       listItem.innerHTML = `EPISODE - ${objFilm.episodeId} </br></br>  NAME -  '${objFilm.name}' </br></br>  SUMMARY - '${objFilm.openingCrawl}' </br>`;
//       const actors = objFilm.characters.map((actor) => {
//         return fetch(actor)
//           .then((actorsArr) => actorsArr.json())
//           .then((actorFromArr) => actorFromArr.name);
//       });

//       Promise.all(actors).then((data) => {
//         const actorsBox = document.createElement("p");
//         listItem.append(actorsBox);
//         actorsBox.innerText = `ACTORS: ${data}`;
//       });

//       list.append(listItem);
//     });
//   });

// root.append(list);


