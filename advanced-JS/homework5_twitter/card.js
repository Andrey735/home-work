class Card {
  constructor(name, email, title, text, id) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.text = text;
    this.id = id;
  }

  createElement(elemType, classNames, text = null) {
    const element = document.createElement(elemType);
    if (text) {
      element.textContent = text;
    }
    element.classList.add(...classNames);
    return element;
  }

  render() {
    this.rootDiv = this.createElement("div", ["root-card"]);
    this.rootDiv.id = this.id;
    const ownerBox = this.createElement("div", ["owner-box"]);
    const ownerName = this.createElement("span", ["owner-name"], this.name);
    const ownerEmail = this.createElement("span", ["owner-email"], this.email);
    const postsBox = this.createElement("div", ["post-box"]);
    const postTitle = this.createElement("h4", ["post-title"], this.title);
    const postBody = this.createElement("p", ["post-body"], this.text);
    postsBox.append(postTitle, postBody);
    ownerBox.append(ownerName, ownerEmail);
    this.close();
    this.rootDiv.prepend(ownerBox, postsBox);

    return this.rootDiv;
  }

  close() {
    const closeBtn = this.createElement("button", ["close-btn"], "DELETE");
    closeBtn.classList.add;
    this.rootDiv.append(closeBtn);
    closeBtn.addEventListener("click", async (event) => {
      const currElem = event.target.parentElement;
      await fetch(`https://ajax.test-danit.com/api/json/posts/${currElem.id}`, {
        method: "DELETE",
      });
      this.rootDiv.remove();
    });
  }
}

export default Card;
