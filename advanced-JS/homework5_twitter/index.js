// "use strict";

import Card from "./card.js";

const getUsers = async () => {
  const response = await fetch("https://ajax.test-danit.com/api/json/users");
  const dataUsers = await response.json();
  return dataUsers;
};

const getPosts = async () => {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts");
  const dataPosts = await response.json();
  return dataPosts;
};

const cardShowDisplay = async () => {
  const animation = document.createElement("div");
  animation.classList.add("loader");
  document.body.append(animation);

  const users = await getUsers();
  const posts = await getPosts();
  animation.remove();

  users.forEach((user) => {
    const { name, email, id } = user;
    posts.forEach((post) => {
      const { id: idPost, userId, title, body } = post;
      if (userId === id) {
        const cardPost = new Card(name, email, title, body, idPost);
        document.body.append(cardPost.render());
      }
    });
  });
};

cardShowDisplay();
