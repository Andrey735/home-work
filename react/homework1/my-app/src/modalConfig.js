const modalConfig = [
  {
    id: 1,
    header: "Title-1",
    closeButton: true,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },

  {
    id: 2,
    header: "Title-2",
    closeButton: true,
    text: "consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et .",
  },
];

export default modalConfig;
