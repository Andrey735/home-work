import { Component } from "react";
import "./Modal.scss";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { closeButton, header, text, action, id, closeButtonAction } =
      this.props;
    return (
      <div
        id={id}
        onClick={closeButtonAction}
        className="modal-window__overlay"
      >
        <div onClick={(e) => e.stopPropagation()} className="modal-content">
          <h2 className="modal-content__title">{header}</h2>
          {closeButton ? (
            <button
              onClick={closeButtonAction}
              className="modal-content__close-btn"
            >
              X
            </button>
          ) : null}
          <div className="modal-content__main-content main-content">
            <p className="main-content__text">{text}</p>
            {action}
          </div>
        </div>
      </div>
    );
  }
}
