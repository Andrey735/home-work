import { Component } from "react";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { backgroundColor, text, handleClick, id } = this.props;
    const buttonStyles = {
      margin: "0, auto",
      cursor: "pointer",
      backgroundColor: `${backgroundColor}`,
    };

    return (
      <button data-modalid={id} onClick={handleClick} style={buttonStyles}>
        {text}
      </button>
    );
  }
}
