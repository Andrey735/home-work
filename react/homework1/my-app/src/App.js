import { Component } from "react";
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";
import modalConfig from "./modalConfig";

class App extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
      modalConfiguration: {},
    };
  }

  handleClick = (event) => {
    const modalId = event.target.dataset.modalid;
    const modalDeclaration = modalConfig.find((item) => item.id == modalId);
    this.setState({
      showModal: !this.state.showModal,
      modalConfiguration: {
        ...modalDeclaration,
      },
    });
  };

  closeButtonAction = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    const { closeButton, header, text, id } = this.state.modalConfiguration;
    let action;

    if (id === 1) {
      action = (
        <div className="footer-btns">
          <button className="btn btn_ok">OK</button>
          <button onClick={this.closeButtonAction} className="btn btn_cancel">
            Cancel
          </button>
        </div>
      );
    } else {
      action = (
        <div className="footer-btns">
          <button className="btn btn_agree">Agree</button>
          <button onClick={this.closeButtonAction} className="btn btn_disagree">
            Disagree
          </button>
        </div>
      );
    }

    return (
      <>
        <div
          className="buttons-container"
          style={{ display: "flex", justifyContent: "center", gap: "15px" }}
        >
          <Button
            handleClick={this.handleClick}
            id="1"
            text="Open first modal"
            backgroundColor="rgba(184, 219, 215, 1)"
          />
          <Button
            handleClick={this.handleClick}
            id="2"
            text="Open second modal"
            backgroundColor="rgba(89, 12, 108, 0.3)"
          />
        </div>
        {this.state.showModal && (
          <Modal
            closeButtonAction={this.closeButtonAction}
            id={id}
            closeButton={closeButton}
            header={header}
            text={text}
            action={action}
          />
        )}
      </>
    );
  }
}

export default App;
