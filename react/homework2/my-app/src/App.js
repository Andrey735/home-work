import { Component } from "react";
import ProductList from "./Components/ProductList/ProductList";
import "./App.scss";

class App extends Component {
  constructor() {
    super();
    const storedFavourites = localStorage.getItem("favourites");
    const storedCart = localStorage.getItem("cart");

    this.state = {
      marketProduts: [],
      productsCart: storedCart ? JSON.parse(storedCart) : [],
      favourites: storedFavourites ? JSON.parse(storedFavourites) : [],
    };
  }

  componentDidMount() {
    fetch("./products.json")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ marketProduts: data });
      });
  }

  componentDidUpdate() {
    localStorage.setItem("favourites", JSON.stringify(this.state.favourites));
    localStorage.setItem("cart", JSON.stringify(this.state.productsCart));
  }

  addToCart = (currentProduct, closeButtonFunction) => {
    this.setState({
      productsCart: [...this.state.productsCart, currentProduct],
    });
    closeButtonFunction();
  };

  addToFavourites = (currentProduct) => {
    const favouritesArr = this.state.favourites;
    if (favouritesArr.length > 0) {
      const productCodes = favouritesArr.map((product) => {
        return product.vendorCode;
      });
      console.log(productCodes);
      if (productCodes.includes(currentProduct.vendorCode)) {
        const newArr = favouritesArr.filter(
          (product, index) => product.vendorCode !== currentProduct.vendorCode
        );
        this.setState({
          favourites: newArr,
        });
      } else {
        this.setState({
          favourites: [...favouritesArr, currentProduct],
        });
      }
    } else {
      this.setState({
        favourites: [...favouritesArr, currentProduct],
      });
    }
  };

  render() {
    console.log(this.state.favourites);

    return (
      <>
        <header className="header">
          <a href="#" className="logo-link" chref="#">
            <img className="logo-link_img" src="./logo.png" alt="logo" />
          </a>
          <div className="header_navigaton">
            <div className="cart">
              <a className="link cart-link" href="#">
                <img className="cart_img" src="./cart.png" alt="cart" />
                <span className="cart-length">
                  {this.state.productsCart.length}
                </span>
              </a>
            </div>
            <div className="favourites">
              <a className="link favourites-link" href="#">
                <img
                  className="favourites_img"
                  src="./favourite.png"
                  alt="favourites"
                />
                <span className="favourites-length">
                  {this.state.favourites.length}
                </span>
              </a>
            </div>
          </div>
        </header>
        <ProductList
          favourites={this.state.favourites}
          cartLength={this.state.productsCart.length}
          handleClickModalAction={this.handleClickModalAction}
          products={this.state.marketProduts}
          addToFavourites={this.addToFavourites}
          addToCart={this.addToCart}
        />
      </>
    );
  }
}

export default App;
