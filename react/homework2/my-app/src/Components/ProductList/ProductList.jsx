import { Component } from "react";
import ProductCard from "../ProductCard/ProductCard";
import "./productList.scss";
import modalConfig from "../../modalConfig";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

export default class ProductList extends Component {
  constructor(props) {
    super();
    this.state = {
      showModal: false,
      modalConfiguration: {},
      currentProduct: {},
    };
  }

  handleClickModalAction = () => {
    this.setState({
      showModal: !this.state.showModal,
      modalConfiguration: {
        ...modalConfig,
      },
    });
  };

  getCurrentProduct = (ProductObject) => {
    this.setState({
      currentProduct: ProductObject,
    });
  };

  handleAddtoCart = () => {
    this.props.addToCart(this.state.currentProduct, this.closeButtonAction);
  };

  closeButtonAction = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    const { closeButton, header, text } = this.state.modalConfiguration;
    return (
      <>
        <div className="product-list">
          {this.props.products.map((monitorInfo) => {
            return (
              <ProductCard
                favourites={this.props.favourites}
                closeButton={closeButton}
                closeButtonAction={this.closeButtonAction}
                handleClickModalAction={this.handleClickModalAction}
                key={monitorInfo.vendorCode}
                monitor={monitorInfo}
                getCurrentProduct={this.getCurrentProduct}
                addToFavourites={this.props.addToFavourites}
              />
            );
          })}
        </div>
        {this.state.showModal && (
          <Modal
            succesClick={this.handleAddtoCart}
            closeButtonAction={this.closeButtonAction}
            closeButton={closeButton}
            header={header}
            text={text}
          />
        )}
      </>
    );
  }
}

ProductList.propTypes = {
  favourites: PropTypes.array,
  addToFavourites: PropTypes.func,
};
