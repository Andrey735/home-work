import { Component } from "react";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { backgroundColor, text, handleClickButton, className } = this.props;
    const buttonStyles = {
      margin: "0, auto",
      cursor: "pointer",
      backgroundColor: `${backgroundColor}`,
    };

    return (
      <button
        className={className}
        onClick={handleClickButton}
        style={buttonStyles}
      >
        {text}
      </button>
    );
  }
}
