import { Component } from "react";
import Button from "../Buttons/Button";
import "./productCard.scss";
import PropTypes from "prop-types";

export default class ProductCard extends Component {
  constructor(props) {
    super();
    this.state = {
      isClickedFavouritesButton: false,
    };
  }
  handleClick = () => {
    this.props.handleClickModalAction();
    this.props.getCurrentProduct(this.props.monitor);
  };

  handleButtonFavourites = () => {
    this.props.addToFavourites(this.props.monitor);
    this.setState({
      isClickedFavouritesButton: !this.state.isClickedFavouritesButton,
    });
  };
  render() {
    const favouritesArr = this.props.favourites.map((product) => {
      return product.vendorCode;
    });
    const { name, img, vendorCode, price, color } = this.props.monitor;
    let btnFavourite;
    if (
      this.state.isClickedFavouritesButton ||
      favouritesArr.includes(vendorCode)
    ) {
      btnFavourite = (
        <Button
          handleClickButton={this.handleButtonFavourites}
          className="btn_add-to-favourites"
          text={
            <img
              className="add-to-favourites_img"
              src="./favourites-filled-star-symbol.png"
              alt="remove into favourites"
            />
          }
        />
      );
    } else {
      btnFavourite = (
        <Button
          handleClickButton={this.handleButtonFavourites}
          className="btn_add-to-favourites"
          text={
            <img
              className="add-to-favourites_img"
              src="./star.png"
              alt="add to favourites"
            />
          }
        />
      );
    }
    return (
      <>
        <div className="monitor_box">
          <div className="monitor_img-box">
            <img className="monitor_img" src={img} alt="" />
          </div>
          <div className="monitor_description">
            <h2 className="monitor_name">{name}</h2>
            <span className="monitor_price">₴{price}</span>
            <Button
              className="btn__add-to-cart"
              handleClickButton={this.handleClick}
              text="Add to Cart"
            />
            <i>Color: {color}</i>

            <i className="monitor_code">{vendorCode}</i>
            {btnFavourite}

            {/* {this.state.isClickedFavouritesButton ||
            favouritesArr.includes(vendorCode) ? (
              <Button
                handleClickButton={this.handleButtonFavourites}
                className="btn_add-to-favourites"
                text={
                  <img
                    className="add-to-favourites_img"
                    src="./favourites-filled-star-symbol.png"
                    alt="remove into favourites"
                  />
                }
              />
            ) : (
              <Button
                handleClickButton={this.handleButtonFavourites}
                className="btn_add-to-favourites"
                text={
                  <img
                    className="add-to-favourites_img"
                    src="./star.png"
                    alt="add to favourites"
                  />
                }
              />
            )} */}
          </div>
        </div>
      </>
    );
  }
}

ProductCard.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  favourites: PropTypes.array,
  addToFavourites: PropTypes.func,
  monitor: PropTypes.object,
};
