export const favouritesReducer = (state = [], action) => {
  switch (action.type) {
    case "SET_FAVOURITES": {
      const isExistFavourites = state.some((monitor) => {
        return monitor.vendorCode === action.payload.favouritesId;
      });
      if (isExistFavourites) {
        return state.filter(
          (monitor) => monitor.vendorCode !== action.payload.favouritesId
        );
      }
      return [...state, action.payload.favourites];
    }

    default:
      return state;
  }
};
