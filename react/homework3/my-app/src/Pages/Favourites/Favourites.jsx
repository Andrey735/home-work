import ProductCard from "../../Components/ProductCard/ProductCard";
import "./favourites.scss";

import Modal from "../../Components/Modal/Modal";
import { useSelector } from "react-redux";

const Favourites = ({
  addToCart,
  addToFavourites,
  handleClickModalAction,
  getCurrentProduct,
  showModal,
  closeButtonAction,
}) => {
  const favourites = useSelector((state) => state.favourites);

  return (
    <>
      {favourites.length < 1 ? (
        <h2 className="title">
          <i>You have not selected any product</i>
        </h2>
      ) : (
        <>
          <h2 className="title">
            You added {favourites.length} product to your favourites{" "}
          </h2>
          <div className="favourites-list">
            {favourites.map((product) => {
              return (
                <ProductCard
                  key={product.vendorCode}
                  monitor={product}
                  // addToFavourites={addToFavourites}
                  addToCart={addToCart}
                  // favourites={favourites}
                  getCurrentProduct={getCurrentProduct}
                  handleClickModalAction={handleClickModalAction}
                />
              );
            })}
          </div>
        </>
      )}
      {showModal && (
        <Modal
          succesClick={addToCart}
          closeButtonAction={closeButtonAction}
          // closeButton={closeButton}
          header="Addition"
          text="Do you want to add this item to your cart"
        />
      )}
    </>
  );
};

export default Favourites;
