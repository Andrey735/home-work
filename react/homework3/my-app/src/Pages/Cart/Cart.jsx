import Modal from "../../Components/Modal/Modal";
import ProductCard from "../../Components/ProductCard/ProductCard";
import "./cart.scss";
import ProductCart from "./ProductCart";

const Cart = ({
  productsCart,
  handleClickModalAction,
  showModal,
  closeButtonAction,
  getCurrentProduct,
  removeFromCart,
}) => {
  return (
    <>
      {productsCart.length < 1 ? (
        <h2 className="title">
          <i>Your cart is empty</i>
        </h2>
      ) : (
        <>
          <h2 className="title">
            You added {productsCart.length} product to your cart
          </h2>
          <div className="cart-list">
            {productsCart.map((product) => {
              return (
                <ProductCart
                  handleClickModalAction={handleClickModalAction}
                  getCurrentProduct={getCurrentProduct}
                  closeButtonAction={closeButtonAction}
                  key={product.vendorCode}
                  product={product}
                />
              );
            })}
          </div>
        </>
      )}
      {showModal && (
        <Modal
          succesClick={removeFromCart}
          closeButtonAction={closeButtonAction}
          header="Product Remove"
          text="Are you sure you want to remove this item from your cart?"
        />
      )}
    </>
  );
};

export default Cart;
