import { useSelector } from "react-redux";
import ProductCard from "../ProductCard/ProductCard";
import "./productList.scss";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

const ProductList = ({
  addToCart,
  favourites,
  addToFavourites,
  handleClickModalAction,
  closeButtonAction,
  // showModal,
  getCurrentProduct,
}) => {
  const marketProducts = useSelector((state) => state.marketProducts);
  const showModal = useSelector((state) => state.showModal);
  console.log(marketProducts);

  return (
    <>
      <div className="product-list">
        {marketProducts.map((product) => {
          return (
            <ProductCard
              favourites={favourites}
              // closeButton={closeButton}
              closeButtonAction={closeButtonAction}
              handleClickModalAction={handleClickModalAction}
              key={product.vendorCode}
              monitor={product}
              getCurrentProduct={getCurrentProduct}
              addToFavourites={addToFavourites}
            />
          );
        })}
      </div>
      {showModal && (
        <Modal
          succesClick={addToCart}
          closeButtonAction={closeButtonAction}
          // closeButton={closeButton}
          header="Addition"
          text="Do you want to add this item to your cart"
        />
      )}
    </>
  );
};

export default ProductList;

ProductList.propTypes = {
  favourites: PropTypes.array,
  addToFavourites: PropTypes.func,
};
