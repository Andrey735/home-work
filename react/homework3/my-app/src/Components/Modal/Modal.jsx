import { Component } from "react";
import Button from "../Buttons/Button";
import "./Modal.scss";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { closeButton, header, text, id, closeButtonAction, succesClick } =
      this.props;
    return (
      <div
        id={id}
        onClick={closeButtonAction}
        className="modal-window__overlay"
      >
        <div onClick={(e) => e.stopPropagation()} className="modal-content">
          <h2 className="modal-content__title">{header}</h2>
          {closeButton ? (
            <Button
              className="modal-content__close-btn"
              text="X"
              handleClickButton={closeButtonAction}
            />
          ) : null}
          <div className="modal-content__main-content main-content">
            <p className="main-content__text">{text}</p>

            <div className="footer-btns">
              <Button
                className="btn btn_ok"
                text="Ok"
                handleClickButton={succesClick}
              />

              <Button
                className="btn btn_cancel"
                text="Cancel"
                handleClickButton={closeButtonAction}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  closeButton: true,
};
