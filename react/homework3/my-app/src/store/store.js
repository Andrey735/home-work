import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
const initialState = {
  marketProducts: [],
  showModal: false,
  favourites: [],
  loader: false,
  // productsCart: [],
};

const loaderReducer = (state = initialState.loader, action) => {
  switch (action.type) {
    case "SHOW_LOADER": {
      return true;
    }

    case "HIDE_LOADER": {
      return false;
    }
    default:
      return state;
  }
};

const modalReducer = (state = initialState.showModal, action) => {
  switch (action.type) {
    case "SHOW_MODAL": {
      return !state;
    }
    default:
      return state;
  }
};

// const productsCartReducer = (state = [], action) => {
//   switch (action.type) {
//     case "ADD_TO_CART": {
//       const isExistCart = state.some((monitor) => {
//         return monitor.vendorCode === action.payload.favouritesId;
//       });

//       if (isExistCart) return state;

//       return [...state, action.payload.cartProduct];
//     }
//   }
// };

const favouritesReducer = (state = [], action) => {
  switch (action.type) {
    case "SET_FAVOURITES": {
      const isExistFavourites = state.some((monitor) => {
        return monitor.vendorCode === action.payload.favouritesId;
      });
      if (isExistFavourites) {
        return state.filter(
          (monitor) => monitor.vendorCode !== action.payload.favouritesId
        );
      }
      return [...state, action.payload.favourites];
    }

    default:
      return state;
  }
};

const marketProductsReducer = (state = [], action) => {
  switch (action.type) {
    case "SET_PRODUCTS": {
      console.log(state);
      return action.payload;
    }
    default:
      return state;
  }
};

const reducers = combineReducers({
  marketProducts: marketProductsReducer,
  favourites: favouritesReducer,
  // cart: productsCartReducer,
  showModal: modalReducer,
  loader: loaderReducer,
});

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : (f) => f;

const store = createStore(
  reducers,
  initialState,
  compose(applyMiddleware(thunk), devTools)
);

export default store;
