import { useEffect } from "react";
import { useState } from "react";
import ProductList from "./Components/ProductList/ProductList";
import "./App.scss";
import { Link, Route, Routes } from "react-router-dom";
import Cart from "./Pages/Cart/Cart";
import Favourites from "./Pages/Favourites/Favourites";
// import modalConfig from "./modalConfig";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts, showModal } from "./store/actions";

const App = () => {
  const [productsCart, setProductsCart] = useState([]);
  // const [favourites, setFavourites] = useState([]);

  const favourites = useSelector((state) => state.favourites);
  const loading = useSelector((state) => state.loader);

  // const [marketProducts, setMarketProducts] = useState([]);

  // const [showModal, setShowModal] = useState(false);
  // const [modalConfiguration, setModalConfiguration] = useState({});
  const [currentProduct, setCurrentProduct] = useState({});

  const storedFavourites = localStorage.getItem("favourites");
  const storedCart = localStorage.getItem("cart");

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
    // if (storedFavourites) {
    //   setFavourites(JSON.parse(storedFavourites));
    // }
    // if (storedCart) {
    //   setProductsCart(JSON.parse(storedCart));
    // }
    // getProducts();
  }, []);

  // const getProducts = () => {
  //   fetch("./products.json")
  //     .then((res) => res.json())
  //     .then((data) => {
  //       dispatch(setProducts(data));
  //     });
  // };

  useEffect(() => {
    localStorage.setItem("favourites", JSON.stringify(favourites));
    localStorage.setItem("cart", JSON.stringify(productsCart));
  }, [favourites, productsCart]);

  console.log(productsCart);
  console.log(favourites);

  // const [isInCart, setIsInCart] = false;

  const handleClickModalAction = () => {
    // setShowModal(!showModal);
    // setModalConfiguration({ ...modalConfig });
  };

  const closeButtonAction = () => {
    dispatch(showModal());
    // setShowModal(false);
  };

  const getCurrentProduct = (productObject) => {
    setCurrentProduct(productObject);
  };

  const addToCart = () => {
    const productCodes = productsCart.map((product) => {
      return product.vendorCode;
    });

    if (!productCodes.includes(currentProduct.vendorCode)) {
      setProductsCart([...productsCart, currentProduct]);
      closeButtonAction();
    } else {
      closeButtonAction();
    }
  };

  const removeFromCart = () => {
    const newCartFilter = productsCart.filter(
      (product) => product.vendorCode !== currentProduct.vendorCode
    );

    setProductsCart(newCartFilter);
    closeButtonAction();
  };

  if (loading) {
    return <h1>Loading...</h1>;
  }

  return (
    <>
      {loading && <h1>Loading...</h1>}

      <header className="header">
        <Link to="/" className="logo-link">
          <img className="logo-link_img" src="./logo.png" alt="logo" />
        </Link>
        <div className="header_navigaton">
          <div className="cart">
            <Link to="/cart" className="link cart-link">
              <img className="cart_img" src="./cart.png" alt="cart" />
              <span className="cart-length">{productsCart.length}</span>
            </Link>
          </div>
          <div className="favourites">
            <Link to="/favourites" className="link favourites-link">
              <img
                className="favourites_img"
                src="./favourite.png"
                alt="favourites"
              />
              <span className="favourites-length">{favourites.length}</span>
            </Link>
          </div>
        </div>
      </header>
      <Routes>
        <Route
          path="/"
          element={
            <ProductList
              favourites={favourites}
              addToCart={addToCart}
              // modalConfiguration={modalConfiguration}
              closeButtonAction={closeButtonAction}
              showModal={showModal}
              handleClickModalAction={handleClickModalAction}
              getCurrentProduct={getCurrentProduct}
              currentProduct={currentProduct}
            />
          }
        />
        <Route
          path="favourites"
          element={
            <Favourites
              favourites={favourites}
              addToCart={addToCart}
              // modalConfiguration={modalConfiguration}
              handleClickModalAction={handleClickModalAction}
              closeButtonAction={closeButtonAction}
              showModal={showModal}
              getCurrentProduct={getCurrentProduct}
              currentProduct={currentProduct}
            />
          }
        />
        <Route
          path="cart"
          element={
            <Cart
              productsCart={productsCart}
              handleClickModalAction={handleClickModalAction}
              showModal={showModal}
              closeButtonAction={closeButtonAction}
              getCurrentProduct={getCurrentProduct}
              removeFromCart={removeFromCart}
            />
          }
        />
      </Routes>
    </>
  );
};

export default App;
